# AUS_WA_LeeuwinCurrent
Western Australia, Leeuwin Current running from IndoFloThru along NW coast around south Capes to Southern Ocean

## Roadmap
- visual: map salinity
- quantify: comparative years, 2021-22 La-Nina-Neutral evolution, personal observations in the SW of high-salinity inshore 

## Files
- WASal.Rmd - investigation
- IMOS_URLs.txt - web-page list for data scrape
- /ncdir - alternate data scrape form .nc files

## Description
This document presents the IMOS ""Hourly aggregated times series"" product. 
It contains: 

- Description of the product
- sample plot of TEMP time series by depth range

#Background
IMOS support the Australian National Mooring Network, which is a facility that deploys and maintains ""moored"" oceanographic instruments all around Australia. The facility is independently run by five sub-facilities that are responsible for the QA/QC of the data. The data is stored in ""netCDF CF compliant"" files, 
one file 
per instrument 
per deployment 
per site. 

The files are accessible through [AODN THREDDS server](http://thredds.aodn.org.au/thredds/catalog/IMOS/catalog.html).

# The Problem

Depending on the user, some difficulties in accessing and processing the files could reduce the usability of the data: 

- The data is scattered across multiple files
- There are different levels of QC
- netCDF files are not the preferred format for biologist

So, to generate a long time series of for example, Temperature for one site, requires a relatively high level of expertise, collect many files from the server and concatenate it.

IMOS has decided to invest in the generation of a more "user friendly" products in order to facilitate the usability of the data by non-expert users --> LTSP series

- Aggregated time series: ONE variable from ALL instruments at ONE site
- Hourly time series: ALL variables from ALL instruments at ONE site, binned to 1hr fixed interval
- Gridded time series: ONE variable from ALL instruments at ONE sites binned to 1hr bins and 1m depth bins (more for the deep water moorings)

# The Format

To aggregate all instruments that has been deployed over the time at different depth requires a particular netCDF structure: [Indexed Ragged Array Representation](http://cfconventions.org/cf-conventions/v1.6.0/cf-conventions.html#_indexed_ragged_array_representation). 

This representation breaks the standard "rectangular" structure that has TIME as a common dimension with another structure that is indexed by "INSTRUMENT" and has "OBSERVATION" as a common dimension. `TIME` is now a variable in the file.

This format have some pros and cons:
 
 PROS:
 - all values from all deployments are in one single file "mixed"
 
 
 CONS: 
 - Not having `TIME` as a dimension require and understanding of the format and some "advanced" techniques to manipulate the data !!!
 
Basically, the process to generate an Hourly aggregated time series is as follow:
1. Check the file for entry conditions: site_code, dates, depth, dimensions, etc
2. Discard out of the water records
3. Count QC flags -> indicator of the quality of the output
4. For each variable, resample the values into 1hr bins
    - Mean/median plus min, max, std, count
5. Add metadata
6. Save file

# The file structure

As mentioned before, all the deployments with its particular `NOMINAL_DEPTH` are aggregated into a single file. When integrating several deployments, you need to consider the different depths, so it is not possible to produce a continuous time series over a common time line. That is why the ragged array structure. 

## Source / Resources
http://thredds.aodn.org.au/thredds/dodsC/IMOS/eMII/demos/timeseries_products/hourly_timeseries/IMOS_ANMN-QLD_BFOSTUZ_20071029_GBRPPS_FV02_hourly-timeseries-including-non-QC_END-20191120_C-20200523.nc' # URL to data

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Contributing
I am 'cleaning up' all my projects/files towards publishing a paper, allowing me to apply for further studies and grants etc.. all help is appreciated. 

## Authors and acknowledgment
Lets work together!

## License
Private invitation collaborations towards publishing, but always open to sharing techniques/code.

## Project status
Ongoing
