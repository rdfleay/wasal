---
title: "Hourly TS aggregation: plot example"
author: 'E. Klein'
date: "2019-10-23"
output: 
  html_document:
    toc:  TRUE
    toc_float: TRUE
    theme: united
    highlight: tango
    code_folding: hide
editor_options: 
  chunk_output_type: console
---



```{r setup, cache = F, echo = F, message = F, warning = F, tidy = F}
# To set global options that apply to every chunk in your file, call knitr::opts_chunk$set 
#   ie, knitr::opts_knit$set(root.dir = "~/Documents/Study/WAdata/ElNino")
#     knitr::opts_chunk$set(echo = TRUE, opts_knit$set(root.dir = "~/Documents/Study/WAdata/ElNino")

# cache = F, cache is to save and reload R objects. We do not recommend that you set the chunk option     cache = TRUE globally

## FINISHED HTML / PRDF file, printing
# echo = FALSE prevents code, but not the results from appearing in the finished file. This is a useful way to embed figures.
#    message = FALSE prevents messages that are generated by code from appearing in the finished file.
#    warning = FALSE prevents warnings that are generated by code from appearing in the finished.
#    fig.cap = "..." adds a caption to graphical results.
#     include = FALSE prevents code and results from appearing in the finished file. R Markdown still runs the code in the chunk, and the results can be used by other chunks.

require(knitr)
options(width = 100, stringAsFactors=F)
opts_chunk$set(echo =T, message = F, error = F, warning = F, comment = NA,  
               fig.align = 'left',  fig.width = 7.5, fig.height = 6,
               tidy = F, cache.path = '.cache/', fig.path = 'fig/')
               
library(RColorBrewer)
palette(brewer.pal(8, "Set2"))
library(kableExtra)
library(ncdf4)
library(ggplot2)
library(dplyr)
```

Last run `r lubridate::now()`
```{r}
lubridate::now()
```

This document presents the IMOS Hourly aggregated times series product. It contains: 

- Description of the product
- sample plot of TEMP time series by depth range

- Aggregated time series: ONE variable from ALL instruments at ONE site
- Hourly time series: ALL variables from ALL instruments at ONE site, binned to 1hr fixed interval
- Gridded time series: ONE variable from ALL instruments at ONE sites binned to 1hr bins and 1m depth bins (more for the deep water moorings)

## The Format

To aggregate all instruments that has been deployed over the time at different depth requires a particular netCDF structure: [Indexed Ragged Array Representation](http://cfconventions.org/cf-conventions/v1.6.0/cf-conventions.html#_indexed_ragged_array_representation). 

This representation breaks the standard "rectangular" structure that has TIME as a common dimension with another structure that is indexed by "INSTRUMENT" and has "OBSERVATION" as a common dimension. `TIME` is now a variable in the file.




This format have some pros and cons:
 
 PROS:
 - all values from all deployments are in one single file.
 
 
 CONS: 
 - Not having `TIME` as a dimension require and understanding of the format and some "advanced" techniques to manipulate the data
 
Basically, the process to generate an Hourly aggregated time series is as follow:
1. Check the file for entry conditions: site_code, dates, depth, dimensions, etc
2. Discard out of the water records
3. Count QC flags -> indicator of the quality of the output
4. For each variable, resample the values into 1hr bins
    - Mean/median plus min, max, std, count
5. Add metadata
6. Save file




### The file structure

As mentioned before, all the deployments with its particular `NOMINAL_DEPTH` are aggregated into a single file. When integrating several deployments, you need to consider the different depths, so it is not possible to produce a continuous time series over a common time line. That is why the ragged array structure. 

See for example this file from GBR Palm Passage (QLD). You can download the file directly from the THREDDS server:

```{r opendata} 
file = 'http://thredds.aodn.org.au/thredds/dodsC/IMOS/eMII/demos/timeseries_products/hourly_timeseries/IMOS_ANMN-QLD_BFOSTUZ_20071029_GBRPPS_FV02_hourly-timeseries-including-non-QC_END-20191120_C-20200523.nc' # URL to data
nc = nc_open(file) # open 'file' ..data.., call it 'nc'... GE = list of 14
```

the file contains all the variables recorded in this site, plus ancillary statistical variables that describe the binning process.

metadata
```{r metadata}
## see the variables
print(nc)
```

data variables
```{r filestructure}
## see the variables
names(nc$var)
names(nc$dim)
```

Note the dimensions: `INSTRUMENT`, `OBSERVATION`.


`TIME` and `DEPTH` are variables

Note the ancillary statistical variables associated to each Variable of Interest (VoI).

In this particular example, the aggregated files contain quality controlled values *and* no QC values as well. In this case, an additional attribute (*percent_quality_controlled*)specifying the percent of QC values is included for each VoI.

See that `TEMP` has been 100% QCed while `TURB` no:

```{r varattrs}
ncatt_get(nc, "TEMP", attname="percent_quality_controlled") #Reads an attribute from a netCDF file.
ncatt_get(nc, "TURB", attname="percent_quality_controlled") #Name of the attribute to read
# value = 100 percent
ncatt_get(nc, "PSAL", attname="percent_quality_controlled") # temp, turbidity, practical salinity
```
### Time series plot: RAW

First we need to extract the variable values and store it in a data frame. We're interested in `TIME`, `TEMP`, `DEPTH`, and `instrument_index`

```{r makeDF}
df = data.frame("TIME" = ncvar_get(nc, "TIME"), # get var from 'nc'
                "TEMP" = ncvar_get(nc, "TEMP"),
                "PSAL" = ncvar_get(nc, "PSAL"), 
                "DEPTH"= ncvar_get(nc, "DEPTH"), 
                "instrument_index" = ncvar_get(nc, "instrument_index"))
head(df)
```

as `TIME` is load as number of days after 1950-01-01T00:00:00 (according to the `TIME` attributes and IMOS netCDf standards), it is necessary to convert it to a R datetime object:

```{r convertTIME}
df$TIME = as.POSIXct(df$TIME*60*60*24, origin = "1950-01-01T00:00:00") # change GE
```



If we plot `TEMP` along `TIME` variable, we will have """all the instruments mixed together""". remember that in """one particular deployment is frequent to have more than one instrument moored at different depths""".

```{r plotRaw, fig.width=12, fig.height=6} 
plot(df$TIME, df$TEMP, ty="l", col="steelblue", xlab="", ylab="TEMP °C")
# fig.width / height .. great to SET in chunk opt.
```


DATA - access URL ### FILES ALL SAVED

https://catalogue-imos.aodn.org.au/geonetwork/srv/api/records/efd8201c-1eca-412e-9ad2-0534e96cea14

ANALYSIS - example code
https://github.com/aodn/imos-user-code-library/blob/master/R/notebooks/ANMN_LTSP_hourlytimeseries_Demo.Rmd


NOTE
GRIB is a file format for the storage and transport of gridded meteorological data, such as Numerical Weather Prediction model output. It is designed to be self-describing, compact and portable across computer architectures. The GRIB standard was designed and is maintained by the World Meteorological Organization.

nc file extension is used for a data file format that was developed by MathWorks, Inc. These NC files are also known as unidata network common data form files
filename extension are associated with NetCDF (Network Common Data Form) service. NC files are used for storing collected scientific data and related metadata. NC files can be opened with a popular software MATLAB
NetCDF (Network Common Data Form) is a set of software libraries and self-describing, machine-independent data formats that support the creation, access, and sharing of array-oriented scientific data

```{r}

# make new directory, for the URL text file, its URL download files, and R script
#user:~$ cd ~/Documents/Study/WAdata
#user:~/Documents/Study/WAdata$ mkdir WASal

#user:~/Documents/Study/WAdata$ cd WASal
#user:~/Documents/Study/WAdata/WAsal$ nano files
# copy URL's from above ("A1: List of GRB files") into new nano text-file
# save nano file as "files":
## OR ##
# download '.txt. file from IMOS (see top), with alll URL's listed

# 1. Base data, Grib Files 
getwd()
setwd("/home/rdfleay/Documents/Study/WAdata/AODN/AODN_WA_CapNat/WASal")
purrr::walk(
   readLines("IMOS_Moor_HourTS_URLs.txt"), # file downlaoded from IMOS
   function(f) download.file(url = f, destfile = basename(f))
)
```

#### NOT INVOLVED HERE..but could check Sal readings against MJO..NINO etc ###
```{r}
# 2. El Nino index data measure base-data against
# https://statisticsglobe.com/download-file-in-r-example
# Specify URL where file is stored
url <- "https://bmcnoldy.rsmas.miami.edu/tropics/oni/ONI_NINO34_1854-2020.txt"
# Specify destination where file should be saved
destfile <- "/home/rdfleay/Documents/Study/WAdata/ElNino/ONI_NINO34_1854-2020.txt"
# Apply download.file function in R
download.file(url, destfile)
```
### 

1. SETUP - packages

```{r}
# let's just quickly load all libraries we require to start with

library(torch) # deep learning frameworks, built directly on libtorch, PyTorch’s C++ backend. There is no dependency on Python, resulting in a leaner software stack and more straightforward installation,  in-built facility to do automatic differentiation, torch can also be used as an R-native, high-performing, highly-customizable optimization tool, beyond the realm of deep learning
library(tidyverse) # language for solving data science challenges with R code, encompasses the repeated tasks at the heart of every data science project: data import, tidying, manipulation, visualisation, and programming (dplyr, tibble ..etc)
library(stars) #infrastructure for data cubes, array data with labeled dimensions, with emphasis on arrays where some of the dimensions relate to time and/or space
library(starsExtra)
library(viridis) # color schemes for pretty plots
library(ggthemes) # extra themes, scales, and geoms, and functions for and related to ggplot2
library(imputeTS) # statsNA, compare grb sst
library(torchvision) # model
library(coro) # triangin, optimizer

torch_manual_seed(777) # random number generation
```

XXXX
section completed & data saved .RDS

READ - GRIB files using stars. For example:

```{r}
grb_dir <- file.path("/home/rdfleay/Documents/Study/WAdata/WASal/grb_dir")
#setwd("/home/rdfleay/Documents/Study/WAdata/ElNino/grb_dir")
#grb_dir <- getwd() # also works..

read_stars(file.path(grb_dir, "sst189101.grb"))  #construct path to file, read taster/daataset from file/connection
#file.exists("~/Documents/Study/WAData/ElNino/sst189102.grb") # TRUE
read_stars(file.path(grb_dir,"sst192101.grb"))
# GRIB file, 1 attribute - SST – on 2-D grid
# complement what stars tells us with additional info found in the documentation:
# east-west grid points run eastward 0.5ºE to 0.5ºW, north-south grid points run northward 89.5ºS to 89.5ºN.


# stars::read-stars employs 'sf'.. stores simple features as basic R data structures (lists, matrix, vectors, etc.). The typical data structure stores geometric and feature attributes as a data frame with one row per feature. However since feature geometries are not single-valued, they are put in a list-colum
```

COMBINE DATA 1 ## available, Files: grb18912020.rds
all files into single DF in single , adds additional dimension, time, ranging from 1891/01/01 - 2020/01/12
```{r}
grb <- read_stars(
  file.path(grb_dir, map(readLines("files", warn = FALSE), basename)), along = "time") %>%
  stars::st_set_dimensions(3,
                    values = seq(as.Date("1891-01-01"), as.Date("2020-12-01"), by = "months"),
                    names = "time"
                    )

# Save an object to a file
saveRDS(grb, file = "grb18912020.rds")
# Restore the object
grb <- readRDS(file = "grb18912020.rds")
```

## R Markdown

This is an R Markdown document. Markdown is a simple formatting syntax for authoring HTML, PDF, and MS Word documents. For more details on using R Markdown see <http://rmarkdown.rstudio.com>.

When you click the **Knit** button a document will be generated that includes both content as well as the output of any embedded R code chunks within the document. You can embed an R code chunk like this:

```{r cars}
summary(cars)
```

## Including Plots

You can also embed plots, for example:

```{r pressure, echo=FALSE}
plot(pressure)
```

Note that the `echo = FALSE` parameter was added to the code chunk to prevent printing of the R code that generated the plot.
